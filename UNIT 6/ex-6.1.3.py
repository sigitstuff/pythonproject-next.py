import tkinter as tk

# Create a window
window = tk.Tk()
window.title("tk")

# Create a label with red text
label = tk.Label(window, text="CLICK THE BUTTON IF YOU DARE", fg="red")
label.pack()

# Function to load and display the image
def show_image():
    # Load the image
    image = tk.PhotoImage(file="UNIT 6/image.png")

    # Display the image in a label
    image_label.config(image=image)
    image_label.image = image

# Create a button
button = tk.Button(window, text="DO IT", command=show_image)
button.pack()

# Create a label to display the image
image_label = tk.Label(window)
image_label.pack()

# Run the window
window.mainloop()
