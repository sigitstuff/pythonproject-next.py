class octopus:
    def __init__(self):
        self.name = "octo"
        self.age = 8
    
    def birthday(self):
        self.age += 1
    
    def get_age(self):
        return self.age
    

def main():
    o1 = octopus()
    o2 = octopus()
    o2.birthday()
    print(o1.get_age())
    print(o2.get_age())

main()