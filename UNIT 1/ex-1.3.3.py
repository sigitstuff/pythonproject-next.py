def is_funny(string):
    return not any([char for char in string if char not in ('h', 'a')])


print(is_funny("hahahahahaha"))
