def stop_iteration_error():
    my_list = [1, 2, 3]
    my_iterator = iter(my_list)
    while True:
        print(next(my_iterator))

def zero_division_error():
    a = 10
    b = 0
    c = a / b

def assertion_error():
    x = 5
    y = 10
    assert x < y, "x is not less than y"

def import_error():
    import some_module_that_does_not_exist

def key_error():
    my_dict = {'a': 1, 'b': 2, 'c': 3}
    value = my_dict['d']

def syntax_error():
    eval('x === 5')

def indentation_error():
    if True:
    print("Indentation error")

def type_error():
    x = 5
    y = '10'
    z = x + y

   
