class UnderAge(Exception):
    def __init__(self, age):
        self.age = age
        
    def __str__(self):
        return "You are under 18 years old. Your age is {}. You can come to the party in {} years.".format(self.age, 18 - self.age)

def send_invitation(name, age):
    try:
        if int(age) < 18:
            raise UnderAge(age)
        else:
            print("You should send an invite to " + name)
    except UnderAge as e:
        print(e)

send_invitation("amit", 20)