import string

class UsernameContainsIllegalCharacter(Exception):
    def __init__(self, character, index):
        self.character = character
        self.index = index
    
    def __str__(self):
        return f"Username contains illegal character '{self.character}' at index {self.index}"

class UsernameTooShort(Exception):
    def __str__(self):
        return "Username is too short"

class UsernameTooLong(Exception):
    def __str__(self):
        return "Username is too long"

class PasswordMissingCharacter(Exception):
    def __str__(self):
        return "Password is missing a character."

class PasswordMissingUppercase(PasswordMissingCharacter):
    def __str__(self):
        return "Password is missing an uppercase letter."
    
class PasswordMissingLowercase(PasswordMissingCharacter):
    def __str__(self):
        return "Password is missing a lowercase letter."
    
class PasswordMissingDigit(PasswordMissingCharacter):
    def __str__(self):
        return "Password is missing a digit."
    
class PasswordMissingSpecial(PasswordMissingCharacter):
    def __str__(self):
        return "Password is missing a special character."

class PasswordTooShort(Exception):
    def __str__(self):
        return "Password is too short"

class PasswordTooLong(Exception):
    def __str__(self):
        return "Password is too long"

def check_input(username, password):
    try:
        if not all(c.isalnum() or c == '_' for c in username):
            for i, c in enumerate(username):
                if not c.isalnum() and c != '_':
                    raise UsernameContainsIllegalCharacter(c, i)
        if len(username) < 3:
            raise UsernameTooShort()
        if len(username) > 16:
            raise UsernameTooLong()

        if len(password) < 8:
            raise PasswordTooShort()
        if len(password) > 40:
            raise PasswordTooLong()

        if not any(c.isupper() for c in password):
            raise PasswordMissingUppercase()
        if not any(c.islower() for c in password):
            raise PasswordMissingLowercase()
        if not any(c.isdigit() for c in password):
            raise PasswordMissingDigit()
        if not any(c in string.punctuation for c in password):
            raise PasswordMissingSpecial()

        print("OK")

    except UsernameContainsIllegalCharacter as e:
        print(e)

    except UsernameTooShort as e:
        print(e)

    except UsernameTooLong as e:
        print(e)

    except PasswordTooShort as e:
        print(e)

    except PasswordTooLong as e:
        print(e)

    except PasswordMissingUppercase as e:
        print(e)

    except PasswordMissingLowercase as e:
        print(e)

    except PasswordMissingDigit as e:
        print(e)

    except PasswordMissingSpecial as e:
        print(e)

def main():
    check_input("A_a1.", "12345678")
    check_input("A_1", "abcdefghijklmnop")
    check_input("A_1", "ABCDEFGHIJLKMNOP")
    check_input("A_1", "ABCDEFGhijklmnop")
    check_input("A_1", "4BCD3F6h1jk1mn0p")

main()